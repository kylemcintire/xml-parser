from xml.dom import minidom

# Parse XML document to create DOM structure
xml_doc = minidom.parse('appgen export BERP.xml')
# Get Users group
user_element = xml_doc.getElementsByTagName('Users')
# Get specific users
user_list = user_element[0].getElementsByTagName('User')
# Create empty dictionary to be filled with user info
users = {}


# Function to add users to dictionary,
# first index is user name to define unique key, the rest is user info
def add_user(user_info):
    users[user_info[0]] = user_info[0:]


# Loop to gather user data from user_list
for my_user in user_list:
    # Clean up username
    name = str(my_user.getAttribute('name')).replace('\\', '')
    # Check GlobalProfile default attribute for admin privileges
    if int(my_user.getElementsByTagName('GlobalProfile')[0].getAttribute('default')) == 1:
        global_profile = 'ADMIN'
    else:
        global_profile = 'USER'

    # Start building user info string
    my_user_info = [name, global_profile]

    # Gather profile elements for the user
    profiles = my_user.getElementsByTagName('Profile')
    # Loop through profiles to get profile specific information
    for profile in profiles:
        profile_attributes = profile.attributes.items()
        for attribute in profile_attributes:
            # Convert attribute unicode to strings for comparison
            first_attribute = str(attribute[0])
            second_attribute = str(attribute[1])

            # Check if user has privilege or not
            if first_attribute == 'appName':
                my_user_info.append(first_attribute + " = " + second_attribute)
            elif second_attribute == '0':
                my_user_info.append(str(attribute[0] + " = " + 'NO'))
            else:
                my_user_info.append(str(attribute[0] + " = " + 'YES'))

    # Pass user info to add_user function
    add_user(my_user_info)

user_input = raw_input('Search user privileges or user type? (PRIVILEGE or TYPE): ')
if user_input.upper() == 'TYPE':
    user_input_type = raw_input('Search for users or admins? (USER or ADMIN): ')
    for name, values in users.iteritems():
        if values[1] == user_input_type.upper():
            print name
elif user_input.upper() == 'PRIVILEGE':
    user_input_privilege = raw_input('Enter the username:' )
    print users[user_input_privilege]

# print users['ANN_RICHARDS']